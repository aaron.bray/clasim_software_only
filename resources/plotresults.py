#!/usr/bin/env python

### Run script in directory containing the log files

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import sys
import pandas as pd
import os


def get_patient_logfile(patient_name, name = "Pulse"):
    """
    Returns file name as a string
    """
    #Get all text files in directory
    text_files = [text_file for text_file in os.listdir(".") if text_file.endswith(".txt")]

    # Filter out specific patient files
    patient_files = [patient for patient in text_files if patient_name in patient]

    # Grab log file that has either (name: "Pulse" or "Simulation") in filename
    patient_logfile = [ file_ for file_ in patient_files if name in file_ ]
    patient_logfile = "".join(map(str, patient_logfile))

    return patient_logfile


# function: to read the pulse log file
def read_pulse_logfile(file_name):
    
    return pd.read_csv(file_name, 
						names = ["Time", "HeartRate", "B_Volume","B_Pressure"], 
						header=0) 


# function: to read and parse the simulation log file
def read_sim_logfile(file_name, entity=None, event=None, filtered=False):

    """function returns a filtered data frame based on entity and event provided
       also filtered parameter has to be set to True otherwise return entire
	   unfiltered data frame
    """
    df = pd.read_csv(file_name)

    if filtered and entity and event:
        return df[(df["Entity"] == entity.title() ) 
		           & (df["Event"] == event.upper())
				 ]
    else:
        return df


# get patient data for 'patient'
def get_patient_data(patient):

	# conversion from seconds to minutes
	SECS_PER_MIN = 60

	# patient data dictionary
	sim_data = {}
	
	# get log files
	pulse_log_file = get_patient_logfile(patient, "Pulse")
	sim_engine_log_file = get_patient_logfile(patient, "Simulation")
	
	# get patient data
	patient_data =  read_pulse_logfile(pulse_log_file)
	sim_data['patient'] = {"time" : patient_data["Time"].values/SECS_PER_MIN,
						   "MAP"  : patient_data["B_Pressure"].values,
								}


	# filtered data from simulation log file
	monitor_data = read_sim_logfile(sim_engine_log_file, 
										"monitor", 
										"receive", 
										True)
										
	sim_data["monitor"] = {"time" : monitor_data["Time"].values/SECS_PER_MIN,
								"MAP" : monitor_data["BloodPressure"].values,
								"HR"  : monitor_data["HeartRate"].values,
								}


	# get input data for controller
	controller_data = read_sim_logfile(sim_engine_log_file, 
										"controller", 
										"receive", 
										True)
										
	sim_data["control-input"] = {"time" : controller_data["Time"].values/SECS_PER_MIN,
								"MAP" : controller_data["BloodPressure"].values,
								}

	# get controller action data
	controller_data = read_sim_logfile(sim_engine_log_file, 
										"controller",
										"rate_change", 
										True)
										
	sim_data["control-actions"] = {"time" : controller_data["Time"].values/SECS_PER_MIN,
								"rate-change" : controller_data["Rate"].values,
								}
	
	# take care of controller-pump interaction logging bug
	sim_data["control-actions"]["rate-change"][0] = \
		sim_data["control-actions"]["rate-change"][1]	

	# get pump data
	pump_data = read_sim_logfile(sim_engine_log_file, 
									"Pump Norepinephrine", 
									"rate_change", 
									True)
									
	sim_data["pump"] = {"time" : pump_data["Time"].values/SECS_PER_MIN,
								"rate-change" : pump_data["Rate"].values,
								}
	
	print(sim_data["pump"])
	# return the simulation data
	return sim_data

# plot patient data
def plot_data(patient, sim_data, separate=True):

	matplotlib.rcParams.update({'font.size': 10})

	# Plot all data (one-axis per entity)
	fig = plt.figure()
	fig.canvas.set_window_title(patient)
	if separate:
		plot_data_separate(sim_data)
	else:
		plot_data_condensed(sim_data)
	
	# fig.show()

def plot_data_separate(sim_data):
	
	# global plot parameters
	num_entities = 4
	
	pump_plot_num = 1
	controller_plot_num = 2
	monitor_plot_num = 3
	patient_plot_num = 4
	
	time_min = 0
	time_max = 65
	
	MAP_min = 65
	MAP_max = 95
	
	rate_min = 0
	rate_max = 90
	
	cla_not_started_x_range = [0, sim_data["monitor"]["time"][0]]
	
	# pump plot
	ax = plt.subplot(num_entities, 1, pump_plot_num)
	
	sim_data["pump"]["time"] = np.append(sim_data["pump"]["time"], [60.0])
		
	sim_data["pump"]["rate-change"] = \
		np.append(sim_data["pump"]["rate-change"], 
					[sim_data["pump"]["rate-change"][-1]])
	
	print(sim_data["pump"])
	
	plt.step(sim_data["pump"]["time"],
			 sim_data["pump"]["rate-change"], 
			 'r',
			 where='post'
			 )
	
	# fill area where patient is not yet in ICU
	ax.fill_between(cla_not_started_x_range,
					rate_min, 
					rate_max,
					facecolor="tab:gray",
					alpha=0.25) 
					
	# add text about CLA not started
	text_x = 1 # min
	text_y = 20 # ml/hr
	ax.text(text_x,text_y, "patient not in ICU\nCLA not started",)
	
	plt.xlim(time_min, time_max)
	plt.ylim(rate_min, rate_max)
	
	plt.title("Pump Actions")
	plt.ylabel("Infusion\nRate\n(ml/hr)")
	
	plt.grid()
	
	# algorithm plot
	# inputs
	algo_input_ax = plt.subplot(num_entities, 1, controller_plot_num)
	algo_input_ax.plot(sim_data["control-input"]["time"],
						sim_data["control-input"]["MAP"], '.')
	
	algo_input_ax.set_xlim(time_min, time_max)
	algo_input_ax.set_ylim(MAP_min, MAP_max)
	
	algo_input_ax.set_title("Algorithm Inputs (left y axis) and Actions (right y axis)")
	algo_input_ax.set_ylabel("MAP\n(mmHg)")
	
	algo_input_ax.legend(['inputs to algorithm'], 
							loc="upper left",
							fontsize="x-small")
							
	algo_input_ax.fill_between(cla_not_started_x_range,
					MAP_min,
					MAP_max,
					facecolor="tab:gray",
					alpha=0.25) 
	
	
	# actions
	algo_action_ax = algo_input_ax.twinx()
	# print(sim_data["control-actions"])
	algo_action_ax.stem(sim_data["control-actions"]["time"],
						sim_data["control-actions"]["rate-change"],
						'r',
						markerfmt='*r',
						basefmt='None')
											
	algo_action_ax.set_xlim(time_min, time_max)
	algo_action_ax.set_ylim(rate_min, rate_max)
	algo_action_ax.set_ylabel("Infusion\nRate\n(ml/hr)")				
		
	algo_action_ax.legend(['commands to pump'],
							loc="lower left",
							fontsize="x-small")
	
	# monitor plot
	ax = plt.subplot(num_entities, 1, monitor_plot_num)	
	plt.plot(sim_data["monitor"]["time"],sim_data["monitor"]["MAP"], '.')
	
	plt.xlim(time_min, time_max)
	plt.ylim(MAP_min, MAP_max)
	
	plt.title("Patient Monitor (Data Observed)")
	plt.ylabel("MAP\n(mmHg)")
	
	ax.fill_between(cla_not_started_x_range,
					MAP_min,
					MAP_max,
					facecolor="tab:gray",
					alpha=0.25)

	
	
	plt.grid()
	
	# patient plot
	target_max = 80
	target_min = 75
	ax = plt.subplot(num_entities, 1, patient_plot_num)
	plt.plot(sim_data["patient"]["time"],sim_data["patient"]["MAP"], 'k')
	
	plt.xlim(time_min, time_max)
	plt.ylim(MAP_min, MAP_max)
	
	target_max_line = ax.axhline(y=target_max)
	target_min_line = ax.axhline(y=target_min)
	
	xdata = np.array(target_max_line.get_xdata())*time_max
	# print(xdata)
	
	ax.fill_between(xdata, 
					target_max_line.get_ydata(),
					target_min_line.get_ydata(),
					facecolor='green',
					alpha=0.25)
					
	ax.fill_between(cla_not_started_x_range,
					MAP_min,
					MAP_max,
					facecolor="tab:gray",
					alpha=0.25) 
					
	plt.title("Patient Data")
	plt.xlabel("time (min)")
	plt.ylabel("MAP\n(mmHg)")
	
	plt.grid()
	
	plt.tight_layout()
	plt.show()
	

def plot_data_condensed(sim_data):
	# global plot parameters
	num_entities = 2
	
	pump_plot_num = 1
	patient_plot_num = 2
	
	time_min = 0
	time_max = 65
	
	MAP_min = 65
	MAP_max = 95
	
	rate_min = 0
	rate_max = 90
	
	cla_not_started_x_range = [0, sim_data["monitor"]["time"][0]]
	
	# pump plot
	ax = plt.subplot(num_entities, 1, pump_plot_num)
	
	sim_data["pump"]["time"] = np.append(sim_data["pump"]["time"], [60.0])
		
	sim_data["pump"]["rate-change"] = \
		np.append(sim_data["pump"]["rate-change"], 
					[sim_data["pump"]["rate-change"][-1]])
	
	# print(sim_data["pump"])
	
	plt.step(sim_data["pump"]["time"],
			 sim_data["pump"]["rate-change"], 
			 'r',
			 where='post'
			 )
	
	# fill area where patient is not yet in ICU
	ax.fill_between(cla_not_started_x_range,
					rate_min, 
					rate_max,
					facecolor="tab:gray",
					alpha=0.25) 
					
	# add text about CLA not started
	text_x = 1 # min
	text_y = 20 # ml/hr
	# ax.text(text_x,text_y, "patient not in ICU\nCLA not started",)
	
	plt.xlim(time_min, time_max)
	plt.ylim(rate_min, rate_max)
	
	plt.title("CLA Reaction")
	plt.ylabel("Infusion\nRate\n(ml/hr)")
	
	plt.grid()
	
	
	# patient plot
	target_max = 80
	target_min = 75
	ax = plt.subplot(num_entities, 1, patient_plot_num)
	plt.plot(sim_data["patient"]["time"],sim_data["patient"]["MAP"], 'k')
	
	plt.xlim(time_min, time_max)
	plt.ylim(MAP_min, MAP_max)
	
	target_max_line = ax.axhline(y=target_max)
	target_min_line = ax.axhline(y=target_min)
	
	xdata = np.array(target_max_line.get_xdata())*time_max
	# print(xdata)
	
	ax.fill_between(xdata, 
					target_max_line.get_ydata(),
					target_min_line.get_ydata(),
					facecolor='green',
					alpha=0.25)
					
	ax.fill_between(cla_not_started_x_range,
					MAP_min,
					MAP_max,
					facecolor="tab:gray",
					alpha=0.25) 
					
	plt.title("Patient Data")
	plt.xlabel("time (min)")
	plt.ylabel("MAP\n(mmHg)")
	
	plt.grid()
	
	plt.tight_layout()
	plt.show()

		
# ------------------------------------------------------------------------------
def print_usage():
	print("usage: script options patients")
	print("\toptions:")
	print("\t\t-seperate\tPlot all data")
	print("\t\t-condense\tPlot only patient data and pump action")

if __name__ == "__main__":

	option_end = 2
	separate = True
	if len(sys.argv) > option_end:
		if(sys.argv[option_end-1] == "-separate"):
			separate = True
		elif(sys.argv[option_end-1] == "-condense"):
			separate = False
		else:
			print_usage()
			sys.exit(0)
			
		patients = sys.argv[option_end:]
				
		print(patients)
		
		for patient in patients:
			print(patient)
			sim_data = get_patient_data(patient)
			plot_data(patient, sim_data, separate)
			
		
	else:
		print_usage()
		sys.exit(0)
		
	


