#include "Controller.h"

//Default constructor
Controller::Controller(): MIN_PERIOD_SECS(0.02) {

}


// Overloard Constructor
Controller::Controller(CMD::Environment* env, PhysiologyData* data, std::map<string, Pump*>& pumps ) : m_env(env),
		m_data(data), m_pumps(pumps), MIN_PERIOD_SECS(0.02) {

			// Initalize algorithm configuration variables
			currentNorepiInfusionRate = 0;
			SalineInfusionRate = 50.0;
			maxNorepinephrineInfusionRate = 84;
			targetMAPRangeMax = 80.0;
			targetMAPRangeMin = 75.0;
			MAPIncreaseThreshold = 80;
			percentOfRate = 0.75;
			currentMAP = 0;

			/* The first action the controller should take is initialize infusions */
			respondToData = &Controller::initializeInfusions;


}


void Controller::update(std::unique_ptr<PhysiologyEngine>& engine) {

		// check controller input rate
		if (m_env->time_index % input_period == 0) {

				cout<<"++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n";
				cout<<"Controller receives data from Monitor at: "<<engine->
					GetSimulationTime(TimeUnit::s)<<"s\n";

				// Gets data from Monitor
			  GetData();
				m_env->logger->LogData(m_env->sim_currentTime,"Controller",
						  "RECEIVE","Received data from Monitor BP", patient_state.at(m_env->vector_index-1) );

				// Get the current MAP value
				currentMAP = patient_state.at(m_env->vector_index-1).pressure;

				/* execute the algorithm to respond to the data */
				((this)->*respondToData)();



		} // End check input rate condition

}; // End Controller update()


void Controller::LoadConfig(std::unique_ptr<PhysiologyEngine>& engine, const std::shared_ptr<Config>& cf) {


		  // Extract the rate and assign it to m_rate_Hz
		  controller_period_secs = cf->lookup("controller.period");

		  Setting& root = cf->getRoot();

		  Setting& enabled = root["controller"]["patientData"]["enabled"];

		  const char* value;

		  vector<string> p_data;

		  for (Setting& vital : enabled) {

					value = vital;
					p_data.push_back(string(value));


		  }


			// Activate patient variables Physiology Data
			m_data->HeartRate = (find(p_data.begin(), p_data.end(), "HeartRate") != p_data.end()) ? true : false;
			m_data->BloodVolume = (find(p_data.begin(), p_data.end(), "BloodVolume") != p_data.end()) ? true : false;
			m_data->BloodPressure = (find(p_data.begin(), p_data.end(), "BloodPressure") != p_data.end()) ? true : false;



		  time_step = engine->GetTimeStep(TimeUnit::s);
		  //MAX_RATE_Hz = 1/time_step;

			WAIT_10_MIN = cf->lookup("controller.algorithm.wait_10");
			WAIT_5_MIN = cf->lookup("controller.algorithm.wait_5");

			WAIT_10_MIN = (60 * WAIT_10_MIN)/time_step; // 10 min
			WAIT_5_MIN = (60 * WAIT_5_MIN)/time_step;  // 5 min

		  // integer input rate for controller
		  input_period = controller_period_secs / time_step;
		  //is_input_multiple = fmod(MAX_RATE_Hz, controller_rate_Hz) == 0.0;
			is_input_multiple = ((int)(controller_period_secs*1000))
														% ((int)(MIN_PERIOD_SECS*1000)) == 0;


		  if(controller_period_secs < MIN_PERIOD_SECS) {

					cout <<"\nController rate is greater than MAX: 50.0\n\n";
					Stop();
		  }

		  else if(!is_input_multiple) {

					cout <<"\nController rate is not a multiple of MAX: 50.0\n\n";
					Stop();
		  }


}


void Controller::SendValues(PhysiologyData& data) {

			//patient_state.emplace_back(value);
			patient_state.push_back(data);

}


void Controller::GetData() {

	// The following are activated in the config file if controller
	//  requires them and set to NaN if not required

			cout<<"CONTROLLER DATA\n";

			if (m_data->HeartRate){

					cout<< "Heart Rate:        "<<patient_state.at(m_env->
					vector_index-1).heart_rate<<endl;
			}
			else {

					patient_state.at(m_env->
					vector_index-1).heart_rate=numeric_limits<double>::quiet_NaN();

			}

			if (m_data->BloodPressure) {

					cout<< "Pressure:          "<<patient_state.at(m_env->
					vector_index-1).pressure<<endl;
			}
			else {

					patient_state.at(m_env->
					vector_index-1).pressure=numeric_limits<double>::quiet_NaN();

			}

			if (m_data->BloodVolume) {

					cout<< "Blood Volume:      "<<patient_state.at(m_env->
					vector_index-1).blood_volume<<endl;
			}
			else {

					patient_state.at(m_env->
					vector_index-1).blood_volume=numeric_limits<double>::quiet_NaN();

			}
			cout<<"++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n";




}




// initialise static variable
vector<PhysiologyData> Controller::patient_state;
//const int Controller::array_sz = 4;

void Controller::Stop() {

		throw "Error";

}


void Controller::SetRate(int rate, unsigned int time_index) {

			//		Pump::ChangePumpRate(rate, m_env->time_index );

}


// Destructor
Controller::~Controller() {

		// clear container when done
		patient_state.clear();

}

//Algorithm functions------------------------
/*  Initialize in "Simulation Plan" */
void Controller::initializeInfusions(void){


		currentNorepiInfusionRate = maxNorepinephrineInfusionRate;

		// log rate
		m_env->logger->LogRate(m_env->sim_currentTime,"Controller",
			 "RATE_CHANGE","Controller sends Command to saline Pump",SalineInfusionRate);
		m_env->logger->LogRate(m_env->sim_currentTime,"Controller",
					 "RATE_CHANGE","Controller sends Command to saline Pump",currentNorepiInfusionRate);

		m_pumps["saline"]->ChangePumpRate(SalineInfusionRate, m_env->time_index);
		m_pumps["bpdrug"]->ChangePumpRate(currentNorepiInfusionRate, m_env->time_index);


		// set next update time
		nextUpdateTime = m_env->time_index + WAIT_10_MIN;
		respondToData = &Controller::waitForMAPIncrease;
}

/* Wait for MAP increase in "Simulation Plan" */
void Controller::waitForMAPIncrease(void){

		if(m_env->time_index == nextUpdateTime) {

				if(currentMAP > MAPIncreaseThreshold) {

						currentNorepiInfusionRate = percentOfRate * maxNorepinephrineInfusionRate;

						m_env->logger->LogRate(m_env->sim_currentTime,"Controller",
									 "RATE_CHANGE","Controller sends Command to saline Pump",currentNorepiInfusionRate);

						m_pumps["bpdrug"]->ChangePumpRate(currentNorepiInfusionRate, m_env->time_index);

						nextUpdateTime = m_env->time_index + WAIT_10_MIN;
						respondToData = &Controller::ensureMAPDeclineAfterIncrease;
				}

				else {
						nextUpdateTime = m_env->time_index + WAIT_5_MIN;
				}

		}

}

/* Ensure MAP decline after increase in "Simulation Plan" */
void Controller::ensureMAPDeclineAfterIncrease(void){
		if(m_env->time_index == nextUpdateTime) {

				if(currentMAP > MAPIncreaseThreshold){

						currentNorepiInfusionRate = percentOfRate * currentNorepiInfusionRate;

						m_env->logger->LogRate(m_env->sim_currentTime,"Controller",
									 "RATE_CHANGE","Controller sends Command to saline Pump",currentNorepiInfusionRate);

						m_pumps["bpdrug"]->ChangePumpRate(currentNorepiInfusionRate, m_env->time_index);
						nextUpdateTime = m_env->time_index + WAIT_10_MIN;

				}

				else {

						nextUpdateTime = m_env->time_index + WAIT_10_MIN;
						respondToData = &Controller::maintainMAPInRange;

				}
		}
}

/* Maintain MAP in range in "Simulation Plan" */
void Controller::maintainMAPInRange(void){
		if(m_env->time_index == nextUpdateTime) {

				if(currentMAP > targetMAPRangeMax){

						currentNorepiInfusionRate = percentOfRate * currentNorepiInfusionRate;

						m_env->logger->LogRate(m_env->sim_currentTime,"Controller",
									 "RATE_CHANGE","Controller sends Command to saline Pump",currentNorepiInfusionRate);

						m_pumps["bpdrug"]->ChangePumpRate(currentNorepiInfusionRate, m_env->time_index);
						nextUpdateTime = m_env->time_index + WAIT_10_MIN;

				}
				else if(currentMAP < targetMAPRangeMin) {

						currentNorepiInfusionRate = percentOfRate * maxNorepinephrineInfusionRate;

						m_env->logger->LogRate(m_env->sim_currentTime,"Controller",
									 "RATE_CHANGE","Controller sends Command to saline Pump",currentNorepiInfusionRate);

						m_pumps["bpdrug"]->ChangePumpRate(currentNorepiInfusionRate, m_env->time_index);
						nextUpdateTime = m_env->time_index + WAIT_5_MIN;

				}

				else {

						nextUpdateTime = m_env->time_index + WAIT_10_MIN;
				}
		}
}
