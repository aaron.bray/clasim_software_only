#include "Monitor.h"


// Constructor
Monitor::Monitor() {

};


//Overload Constructor
Monitor::Monitor(CMD::Environment* env, PhysiologyData* data): m_env(env), m_data(data) {

	//cout << endl << className << endl;
	//className = typeid(*this).name();
};


void Monitor::update(std::unique_ptr<PhysiologyEngine>& engine) {


		// check monitor input rate
		if (m_env->time_index % input_period == 0) {

				 	cout << "\nMonitor received patient data at: " <<
							engine->GetSimulationTime(TimeUnit::s)<<"s\n";

					m_data->heart_rate = engine->GetCardiovascularSystem()->
							GetHeartRate(FrequencyUnit::Per_min);

					m_data->pressure = engine->GetCardiovascularSystem()->
							GetMeanArterialPressure(PressureUnit::mmHg);

					m_data->blood_volume = engine->GetCardiovascularSystem()->
							GetBloodVolume(VolumeUnit::mL);

					//Log data
					m_env->logger->LogData(
							   engine->GetSimulationTime(TimeUnit::s),
							   "Monitor",
							   "RECEIVE",
			  				 "Receive data from patient BP: ",
							   *m_data
							  );




					// check monitor output rate
			  		if (m_env->time_index % output_period == 0) {

								m_env->logger->LogData(
									   engine->GetSimulationTime(TimeUnit::s),
									   "Monitor",
									   "SEND",
					  				   "Send data to controller BP: ",
									   *m_data
									  );

								// Send data to controller
								SendData(*m_data);

					}

		}

}


void Monitor::LoadConfig(std::unique_ptr<PhysiologyEngine>& engine, const std::shared_ptr<Config>& cf) {

	  m_input_period_secs = cf->lookup("monitor.input_period");
	  m_output_period_secs = cf->lookup("monitor.output_period");

	  // Get engine time step/period
	  time_step = engine->GetTimeStep(TimeUnit::s);

		/*Had issues with fmod*/
	  // Check if rate provided in config file is multiple of MIN_PERIOD_SECS
	  //is_input_multiple = fmod(MIN_PERIOD_SECS, m_input_period_secs) == 0.0;
	  //is_output_multiple = fmod(MIN_PERIOD_SECS, m_output_period_secs) == 0.0;

		is_input_multiple = ((int)(m_input_period_secs*1000))
		 										% ((int)(MIN_PERIOD_SECS*1000))== 0;
		is_output_multiple = ((int)(m_output_period_secs*1000))
		 										 % ((int)(MIN_PERIOD_SECS*1000))== 0;

	  // Convert frequence to integer period
	  //i.e. freq:25Hz period:0.04 int_period: (0.04/time_step) = 2
	  input_period = m_input_period_secs / time_step;
	  output_period = m_output_period_secs / time_step;


	  if(m_input_period_secs < MIN_PERIOD_SECS || m_output_period_secs < MIN_PERIOD_SECS ) {

				cout <<"\nOne of the monitor rate values is greater than MAX: 50.0\n\n";
				Stop();
	  }

	  else if(!is_input_multiple || !is_output_multiple ) {

				cout <<"\nOne of the monitor rate values is not a multiple of 50.0\n\n";
				Stop();
	  }

}



void Monitor::SendData( PhysiologyData& p_state) {

			cout<< "MONITOR DATA"<<endl;
			cout<< "Heart Rate:   "<< p_state.heart_rate<<endl;
			cout<< "Pressure:     "<< p_state.pressure<<endl;
			cout<< "Blood Volume: "<< p_state.blood_volume<<endl;

			Controller::SendValues(p_state);
			m_env->vector_index++;

}


void Monitor::Stop() {

			throw "Error";

}


// Destructor
Monitor::~Monitor() {

};
