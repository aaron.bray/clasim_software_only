#include "Pump.h"


// Constructor
Pump::Pump()
{

}


//Overload Constructor
Pump::Pump(CMD::Environment* env, PhysiologyData* data, string substance): m_env(env), m_data(data),
					m_substance(substance), pump_rate(0), LOG_INITIAL_PUMP_RATE(true), pump_data{make_pair(0.0,0ul)} {

						NorepiConcentration = 16;
						SalineBagVolume = 500;


}


void Pump::update(std::unique_ptr<PhysiologyEngine>& engine) {



		if(LOG_INITIAL_PUMP_RATE /*m_env->time_index == 0*/) {

				m_env->logger->LogRate(
							engine->GetSimulationTime(TimeUnit::s),
							"Pump "+m_substance,
							"RATE_CHANGE",
							"Initial Pump rate",
							pump_rate
						);

		}

		LOG_INITIAL_PUMP_RATE = false;
		//cout << "Time index " << m_env->time_index << endl;
		//cout << "Time index passed to pump " << pump_data.back().second << endl;

		if(m_env->time_index - pump_data.back().second == delay_period) {

					// update pump rate
					SetInfusionRate(engine, pump_data.back().first);

					m_env->logger->LogRate(
					   		   engine->GetSimulationTime(TimeUnit::s),
					   		   "Pump "+m_substance,
							   	 "RATE_CHANGE",
							   	 "EXECUTE Command from Controller",
							   	 pump_rate
							  );

					cout<<"Command EXECUTED at: "<< engine->GetSimulationTime(TimeUnit::s)<<"s\n";

		}


	//pump_data.at(m_env->pump_index-1).second
}


void Pump::LoadConfig(std::unique_ptr<PhysiologyEngine>& engine, const
std::shared_ptr<Config>& cf) {


		  // Grab pump settings from config file
		  pump_period_sec = cf->lookup("pump.period");
		  pump_delay = cf->lookup("pump.delay");


		  // Probably make these global
		  time_step = engine->GetTimeStep(TimeUnit::s);

		  //MAX_RATE_Hz = 1/time_step;


		  // integer input rate for controller
		  input_period = pump_period_sec / time_step;
		  delay_period = (pump_delay/ time_step);
		  period = input_period + delay_period;

		  is_input_multiple = ((int)(1000*pump_period_sec))
			 										% ((int)(1000*MIN_PERIOD_SECS)) == 0.0;


		  if(pump_period_sec < MIN_PERIOD_SECS) {

					cout <<"\nPump rate is greater than MAX: 50.0\n\n";
					Stop();
		  }

		  else if(!is_input_multiple) {

					cout <<"\nPump rate is not a multiple of MAX: 50.0\n\n";
					Stop();
		  }


			// Set up substance infusion
		  if(m_substance == "Saline") {
					// instatiate infusion compound
					SESubstanceCompound* saline = engine->GetSubstanceManager().GetCompound(m_substance);
					m_infusion = new SESubstanceCompoundInfusion(*saline);
					//the total volume in the bag of Saline
					m_infusion->GetBagVolume().SetValue(SalineBagVolume, VolumeUnit::mL);
			}

			if(m_substance == "Norepinephrine") {

						SESubstance* bpdrug = engine->GetSubstanceManager().GetSubstance(m_substance);
						bpdrug_infusion = new SESubstanceInfusion(*bpdrug);
						bpdrug_infusion->GetConcentration().SetValue(NorepiConcentration, MassPerVolumeUnit::ug_Per_mL);
			}
}


void Pump::SetInfusionRate(std::unique_ptr<PhysiologyEngine>& eng, double infusion_rate) {


	 		pump_rate = infusion_rate;


			if(m_substance == "Saline") {

					//The rate to admnister the compound in the bag in this case saline
					m_infusion->GetRate().SetValue(pump_rate, VolumePerTimeUnit::mL_Per_hr);
					eng->ProcessAction(*m_infusion);

					cout<< m_substance+"_Pump rate changed to: " << pump_rate <<endl;
					cout<<"++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n";

		  }

			if(m_substance == "Norepinephrine") {

					bpdrug_infusion->GetRate().SetValue(pump_rate, VolumePerTimeUnit::mL_Per_hr);
					eng->ProcessAction(*bpdrug_infusion);

					cout<< m_substance+"_Pump rate changed to: " << pump_rate <<endl;
					cout<<"++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n";

			}

}



void Pump::ChangePumpRate(double new_rate, unsigned long new_time_index){

		pump_data.push_back(make_pair(new_rate, new_time_index));
		//SetInfusionRate(new_rate, new_time_index);
}



void Pump::Stop() {

		throw "Error";
}

//To be used later
//bool Pump::command = false;

//Nolonger static variable
//vector< pair<int, unsigned long> > Pump::pump_data;

void Pump::Clear() {

		m_infusion = nullptr;
		bpdrug_infusion = nullptr;
		// clear data accumulated in te containr after simulation
		pump_data.clear();

}

// Destructor
Pump::~Pump() {

		Pump::Clear();

		if (m_infusion == nullptr){
				delete m_infusion;
		}
		if (bpdrug_infusion == nullptr) {
				delete bpdrug_infusion;
		}

}
