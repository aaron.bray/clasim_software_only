#include "Pump.h"
#include "Monitor.h"
#include "Controller.h"
#include "Environment.h"
#include "PhysiologyData.h"
#include "SimulationLogger.h"
#include "SimEngine.h"


int main() {

	clock_t t1, t2, sim_run_time;
	vector<string> patients_list;
	auto cf = std::make_shared<Config>();// LibConfig instance
	float sim_time_secs, sim_time_mins;
	string f_path = clasim_config_dir+"scenario.cfg";
	const char* filepath = f_path.c_str();

	try
	{
	    cf->readFile(filepath);
	}

	catch(const FileIOException &fioex) {
	    std::cerr << "I/O error while reading file: "<< filepath <<std::endl;
	}

	catch(const ParseException &pex) {
	    std::cerr << "Parse error at " << pex.getFile() << ":"<< pex.getLine()
		    << " - " << pex.getError() << std::endl;
	}

	patients_list = get_global_patients(cf);

	for(auto patient: patients_list) {
	    // start timer
	    t1 = clock();

	    // Simulation input : Name of patient, Config object
	    Simulation(patient, cf);

	    // stop timer
	    t2 = clock();

	    sim_run_time = t2 - t1;
	    sim_time_secs = ((double)sim_run_time)/CLOCKS_PER_SEC;
	    sim_time_mins = sim_time_secs/60.0;

	    cout << "\nThe Simulation for " << patient;
	    cout  << " took "<< sim_time_secs <<" seconds ";
	    cout <<"or " << sim_time_mins <<" minutes"<<endl;
	}


}

void Simulation(const string patient_name, const std::shared_ptr<Config>& cf) {

	// Create the engine and load the patient
	std::unique_ptr<PhysiologyEngine> pe = CreatePulseEngine(clasim_results_dir+
	    "SimulationEngine_"+patient_name+".log");

	pe->GetLogger()->Info("CLA Simulation");
	SEScalarTime startTime;
  // You can optionally specify a specific simulation time for the engine to use as its initial simulation time
  // If no time is provided, the simulation time that is in the state file will be used
  // Note the provided state files are named to include what is simulation time is
  	startTime.SetValue(0, TimeUnit::s);


	if (!pe->SerializeFromFile(pulse_state_dir+patient_name+"@0s.json", JSON, &startTime, nullptr))
	{
	    pe->GetLogger()->Error("Could not load state, check the error");
	    return;
	}

	// Create data requests for each value that should be written to the output log as
	// the engine is executing
	// Physiology System Names are defined on the System Objects
	// defined in the Physiology.xsd file
	pe->GetEngineTracker()->GetDataRequestManager().CreatePhysiologyDataRequest(
	    "HeartRate", FrequencyUnit::Per_min);
	pe->GetEngineTracker()->GetDataRequestManager().CreatePhysiologyDataRequest(
	    "BloodVolume", VolumeUnit::mL);
	pe->GetEngineTracker()->GetDataRequestManager().CreatePhysiologyDataRequest(
	    "MeanArterialPressure", PressureUnit::mmHg);
        // Store requested data in a file
	pe->GetEngineTracker()->GetDataRequestManager().SetResultsFilename(
	    clasim_results_dir+"PulseSimEngine_"+patient_name+".txt");

	// Hemorrhage Action
	SEHemorrhage hemorrhageLeg;
	const double initialMAP = 70.0;
	double currentMAP;
 	double hemorrhageRate;

	// Enviroment object
	CMD::Environment sim_env;
	PhysiologyData data;

	// Load simulation environment/variables
	Global_LoadConfig(pe, cf, &sim_env);

	// Hash map for the different pumps
	map<string, Pump*> pumps;

	// Intialize Saline Pump
	Pump pump_saline(&sim_env, &data, "Saline");
	pump_saline.LoadConfig(pe, cf);

        // Initialize Norepinephrine Pump
	Pump pump_bpdrug(&sim_env, &data, "Norepinephrine");
	pump_bpdrug.LoadConfig(pe, cf);

	pumps.insert(make_pair("saline", &pump_saline));
	pumps.insert(make_pair("bpdrug", &pump_bpdrug));

	// Initialize Controller
	Controller controller(&sim_env, &data, pumps);
	controller.LoadConfig(pe, cf);

	// Initialize Monitor
	Monitor monitor(&sim_env, &data);
	monitor.LoadConfig(pe, cf);

	// logger
	sim_env.logger = std::make_shared<SimulationLogger>(clasim_results_dir+
	    "SimulationEngineLog_"+patient_name+".txt");

	// Execute stop hemmorrhage only once; without this it'd be executed for each
	// timestep
	bool STOP = false;
	bool DEVICES_START = false;

	// Stop when the set time is reached
	while (sim_env.time_index <= sim_env.simulation_timesteps) {


	  sim_env.sim_currentTime = pe->GetSimulationTime(TimeUnit::s);

	  // event_hemorrage_start
	  if(sim_env.time_index == sim_env.simulation_injury_start_timestep) {

		// Hemorrhage Starts
		hemorrhageRate = 100;
		cout << "\nHemorrhage Started at: "<<pe->GetSimulationTime(TimeUnit::s)<<
			endl;

		// location of the hemorrhage
		hemorrhageLeg.SetCompartment(pulse::VascularCompartment::RightLeg);
		// rate of hemorrhage
		hemorrhageLeg.GetRate().SetValue(hemorrhageRate,
						VolumePerTimeUnit::mL_Per_min);
		// Engine processes this action
		pe->ProcessAction(hemorrhageLeg);

		STOP = true;
	    }

	    // polling MAP
	    currentMAP = pe->GetCardiovascularSystem()->GetMeanArterialPressure(
		    PressureUnit::mmHg);

	    if(currentMAP <= initialMAP && STOP) {

		// Hemorrhage Stops
		hemorrhageRate = 0;
		cout<<"\nHemorrhage Stoppd at: "<< pe->GetSimulationTime(TimeUnit::s)<<
		    endl;

		// location of the hemorrhage
		hemorrhageLeg.SetCompartment(pulse::VascularCompartment::RightLeg);
		hemorrhageLeg.GetRate().SetValue(hemorrhageRate,
                                     VolumePerTimeUnit::mL_Per_min);
		pe->ProcessAction(hemorrhageLeg);

		STOP = false;

		// instruct devices to start
		DEVICES_START = true;

		cout << "START DEVICES" << endl;
	    }

	    if (DEVICES_START) {

		monitor.update(pe);
		controller.update(pe);
		pumps["saline"]->update(pe);
		pumps["bpdrug"]->update(pe);

	     }

	    // Track patient data
	    pe->GetEngineTracker()->TrackData(pe->GetSimulationTime(TimeUnit::s));
	    // Advance engine for each time step
	    pe->AdvanceModelTime();
	    // increment environment time index whenever engine advances
	    sim_env.time_index++;

	}// End while loop


}// End Simulation function


void Global_LoadConfig(std::unique_ptr<PhysiologyEngine>& engine, const std::shared_ptr<Config>& cf, CMD::Environment* env) {

	/* get the simulation run time */
	try
	{
	    env->simulation_time = cf->lookup("simulation.time.run");
	}
	catch(const SettingNotFoundException) {
	    cout << "Setting Not Found: simulation.time.run" << endl;
	}

	/* get the injury start time */
	try
	{
	    env->simulation_injury_start = cf->lookup("simulation.time.injury_start");
	}
	catch(const SettingNotFoundException) {
	    cout << "Setting Not Found: simulation.time.injury_start" << endl;
	}

	/* get the injury stop time */
	try
	{
	    env->simulation_injury_stop = cf->lookup("simulation.time.injury_stop");
	}
	catch(const SettingNotFoundException) {
	    cout << "Setting Not Found: simulation.time.injury_stop" << endl;
	}

	env->time_index = 0; // initialize the time index
	env->engine_timestep = engine->GetTimeStep(TimeUnit::s);
	env->simulation_timesteps = env->simulation_time / env->engine_timestep;
	env->simulation_injury_start_timestep = env->simulation_injury_start /
							env->engine_timestep;
	env->simulation_injury_stop_timestep = env->simulation_injury_stop /
							env->engine_timestep;
	env->vector_index = 0;

	env->sim_currentTime = engine->GetSimulationTime(TimeUnit::s);


}// End Global_LoadConfig

vector<string> get_global_patients(const std::shared_ptr<Config>& cf) {

	vector<string> patients;

	try{
	    Setting& names = cf->lookup("simulation.patients.names");
	    // Check if patient list is empty
	    if (names.getLength() < 1) cout <<"Name List is empty"<<endl;

	    for (Setting& name : names) {
		patients.push_back(string((const char*)name));
	    }

	}
	catch(const SettingNotFoundException) {
	    cout << "Setting Not Found: simulation.patients.names" << endl;
	}

	return patients;

}// End get_global_patients
