#ifndef MEDICALDEVICE_H
#define MEDICALDEVICE_H

#include "CommonDataModel.h"
#include "PulsePhysiologyEngine.h"

#include <iostream>
#include <string>
#include <chrono>
#include <libconfig.h++>
#include "PhysiologyData.h"

#include "system/physiology/SECardiovascularSystem.h"
#include "properties/SEScalarFrequency.h"

using namespace std;
using namespace libconfig;


class MedicalDevice {
	protected:

	    double m_input_period_secs; // The rate at which device receives or sends data
	    const double MIN_PERIOD_SECS; // Maximum rate in Hz at which data can be transfered
	    Config m_cfg; // API for accessing configuration files


	public:
	    MedicalDevice();

	    // Device logic will go in here
	    // This function uses the engine object to query required info
	    virtual void update(std::unique_ptr<PhysiologyEngine>&);

	    // Load the configuration file to capture device rate
	    virtual void
LoadConfig(std::unique_ptr<PhysiologyEngine>&, const std::shared_ptr<Config>& cf);

	    // Destructor
	    ~MedicalDevice();
};

#endif
