
#ifndef SIMULATIONLOGGER_H
#define SIMULATIONLOGGER_H

#include <stdio.h>
#include <iostream>
#include <fstream>
#include <string>
#include <typeinfo>
#include<utility>
#include "PhysiologyData.h"


using namespace std;

class SimulationLogger {


	private:

	    const string m_filename;
	    ofstream m_out_file;


	public:

	    SimulationLogger();

	    //Overload constructor
	    SimulationLogger(const string);

	    void LogData(double, string, string, string, PhysiologyData&);

	    void LogRate(double, string, string, string, double);

	    //Destructor
	    ~SimulationLogger();


};



#endif
