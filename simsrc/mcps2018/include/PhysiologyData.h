

#ifndef PHYSIOLOGYDATA_H
#define PHYSIOLOGYDATA_H


struct PhysiologyData {

	double heart_rate;
	double pressure;
	double blood_volume;

        bool HeartRate;
        bool BloodPressure;
        bool BloodVolume;
};


#endif
