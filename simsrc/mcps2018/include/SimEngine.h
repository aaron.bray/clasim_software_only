#ifndef SIMENGINE_H
#define SIMENGINE_H

#include <string>
#include <iostream>
#include <vector>
#include <map>
#include <libconfig.h>
#include <time.h> /* clock_t, clock, CLOCKS_PER_SEC */
#include "configure_clasim_paths.h"

#include "CommonDataModel.h"
#include "PulsePhysiologyEngine.h"

// Include the various types you will be using in your code
#include "engine/SEDataRequestManager.h"
#include "compartment/SECompartmentManager.h"
#include "compartment/fluid/SEGasCompartment.h"
#include "patient/SEPatient.h"
#include "patient/assessments/SEPulmonaryFunctionTest.h"
#include "patient/actions/SEHemorrhage.h"
#include "patient/actions/SESubstanceCompoundInfusion.h"
#include "patient/actions/SESubstanceInfusion.h"
#include "system/physiology/SEBloodChemistrySystem.h"
#include "system/physiology/SECardiovascularSystem.h"
#include "system/physiology/SEEnergySystem.h"
#include "system/physiology/SERespiratorySystem.h"
#include "substance/SESubstanceManager.h"
#include "substance/SESubstance.h"
#include "substance/SESubstanceCompound.h"
#include "engine/SEEngineTracker.h"
#include "engine/SEEventHandler.h"
#include "properties/SEScalar0To1.h"
#include "properties/SEScalarFrequency.h"
#include "properties/SEScalarMassPerVolume.h"
#include "properties/SEScalarPressure.h"
#include "properties/SEScalarTemperature.h"
#include "properties/SEScalarTime.h"
#include "properties/SEScalarVolume.h"
#include "properties/SEScalarVolumePerTime.h"
#include "properties/SEFunctionVolumeVsTime.h"
#include "properties/SEScalarMass.h"
#include "properties/SEScalarLength.h"
#include "engine/SEEventHandler.h"


using namespace libconfig;
using namespace std;

void Simulation(const std::string, const std::shared_ptr<Config>& cf);

void Global_LoadConfig(std::unique_ptr<PhysiologyEngine>&, const
std::shared_ptr<Config>&, CMD::Environment*);

vector<string> get_global_patients(const std::shared_ptr<Config>& );

#endif
