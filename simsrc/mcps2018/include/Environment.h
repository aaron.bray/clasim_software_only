

#ifndef ENVIRONMENT_H
#define ENVIRONMENT_H

#include <memory>
#include <vector>
#include "SimulationLogger.h"

namespace CMD {

		struct Environment {

			double engine_timestep; // engine period
			double simulation_time; // grab from config file in seconds
			unsigned long simulation_timesteps; // how long should simulation run
			unsigned long time_index; // track engine advancement time
			unsigned long vector_index; // track size of vector used to pass data between 						objects ie. vector<PhysiologyData>

			double simulation_injury_start;
			double simulation_injury_stop;

			double sim_currentTime;

			unsigned long simulation_injury_start_timestep;
			unsigned long simulation_injury_stop_timestep;

		  vector<string> patients;

			unsigned long pump_index;

			std::shared_ptr<SimulationLogger> logger;



		};
}

#endif
