#ifndef PUMP_H
#define PUMP_H

#include <iostream>
#include <string>
#include <utility>
#include <libconfig.h++>
#include <limits>

#include "MedicalDevice.h"
#include "Controller.h"
#include "Environment.h"
#include "PhysiologyData.h"

#include "patient/actions/SESubstanceCompoundInfusion.h"
#include "patient/actions/SESubstanceInfusion.h"
#include "substance/SESubstanceManager.h"
#include "substance/SESubstanceCompound.h"
#include "properties/SEScalarMassPerVolume.h"
#include "substance/SESubstance.h"

#include "properties/SEScalarVolume.h"
#include "properties/SEScalarVolumePerTime.h"

using namespace std;
using namespace libconfig;


class Pump : public MedicalDevice {

	private:

		bool is_input_multiple;

		double pump_period_sec;
		double pump_delay;
		double time_step;
		double delay_period;

		unsigned long input_period;
		unsigned long period;
		int pump_rate;

		bool LOG_INITIAL_PUMP_RATE;


		// rate, time_index
		vector< pair<double, unsigned long> > pump_data;

		CMD::Environment* m_env;
		PhysiologyData* m_data;
		string m_substance;

		SESubstanceCompoundInfusion* m_infusion;
		SESubstanceInfusion* bpdrug_infusion;

		double NorepiConcentration;
		double SalineBagVolume;


	public:

	    static bool command;

			//Default Constructor
    	Pump();
	    //Overload Constructor
	    Pump(CMD::Environment*, PhysiologyData*, string);

			void ChangePumpRate(double, unsigned long);

			// Device logic will go in here
	    // This function uses the engine object to query required info
	    virtual void update(std::unique_ptr<PhysiologyEngine>&);

	    // Load all pump configurations
	    virtual void LoadConfig(std::unique_ptr<PhysiologyEngine>&, const std::shared_ptr<Config>&);

	    void SetInfusionRate(std::unique_ptr<PhysiologyEngine>&, double);

	    void Stop();

			void Clear();

	    // Destructor
	    ~Pump();
};

#endif
