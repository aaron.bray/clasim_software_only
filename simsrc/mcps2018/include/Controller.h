#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <iostream>
#include <string>
#include <libconfig.h++>
#include <vector>
#include <algorithm>
#include <limits>
#include <math.h>
#include <cmath>
#include <map>
#include <stdlib.h> // srand()
#include <ctime>

#include "CommonDataModel.h"
#include "PulsePhysiologyEngine.h"
#include "Environment.h"
#include "PhysiologyData.h"
#include "Pump.h"


#include "system/physiology/SECardiovascularSystem.h"
#include "properties/SEScalarTime.h"
#include "properties/SEScalarVolumePerTime.h"

using namespace std;
using namespace libconfig;

class Pump;

class Controller {
	private:

	    double controller_period_secs; // The rate at which device receives or sends data
	    double time_step;
			int new_rate;
	    Config cfg; // API for accessing configuration files
	    const double MIN_PERIOD_SECS;
	    bool is_input_multiple;

			//static const int array_sz;
			int array_rate[4]; // for generating random rates


	    unsigned input_period;
	    static vector<PhysiologyData> patient_state;
	    CMD::Environment* m_env;
      PhysiologyData* m_data;
			std::map<string, Pump*> m_pumps;

			double WAIT_10_MIN;
			double WAIT_5_MIN;
			unsigned long initialize_time_index;
			unsigned long checkmap_time_index;
			unsigned long ensure_map_time_index;
			unsigned long maintain_map_time_index;
			unsigned long else_maintain_map_time_index;

			bool set_initialize_time_index;
			bool set_checkmap_timeindex;
			bool set_ensure_map_time_index;
			bool set_maintain_map_time_index;
			bool set_else_maintain_map_time_index;

			bool INITIALIZED;
			bool checkmap_initialized;
			bool ensure_map_initialized;
			bool maintain_map_initialized;
			bool else_maintain_map_initialized;
			bool DEACTIVATE_INIT;
			bool WAIT_FOR_MAP_INCREASE;
			bool ENSURE_MAP_DECREASE;
			bool MAINTAIN_MAP_RANGE;
			bool STAY_IN_2;
			bool STAY_IN_3;
			bool STAY_IN_4;
			bool ELSE_STAY_IN_4;

;


			//Algorithm stuff
			double currentMAP;
			double currentNorepiInfusionRate;
			double SalineInfusionRate;
			double maxNorepinephrineInfusionRate;
			double percentOfRate;
			double targetMAPRangeMax;
			double targetMAPRangeMin;
			double MAPIncreaseThreshold;
			unsigned long nextUpdateTime;

			/* Pointer to current function to run for algorithm */
			void (Controller::*respondToData)(void);

			/*  Initialize in "Simulation Plan" */
			void initializeInfusions(void);

			/* Wait for MAP increase in "Simulation Plan" */
			void waitForMAPIncrease(void);

			/* Ensure MAP decline after increase in "Simulation Plan" */
			void ensureMAPDeclineAfterIncrease(void);

			/* Maintain MAP in range in "Simulation Plan" */
			void maintainMAPInRange(void);




	public:

            // Constructor
	    Controller(CMD::Environment*, PhysiologyData*, std::map<string, Pump*>&);
	    Controller();

	    // Device logic will go in here
	    // This function uses the engine object to query required info
	    void update(std::unique_ptr<PhysiologyEngine>&);


	    // Load the configuration file to capture controller rate
	    void LoadConfig(std::unique_ptr<PhysiologyEngine>&, const std::shared_ptr<Config>&);

	    static void  SendValues(PhysiologyData&);

	    void GetData();

	    void Stop();

	    void SetRate(int, unsigned int);


	    // Destructor
	    ~Controller();
};

#endif
