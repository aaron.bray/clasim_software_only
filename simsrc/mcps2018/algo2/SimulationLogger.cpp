#include "SimulationLogger.h"


SimulationLogger::SimulationLogger() {

}


//Overload Constructor
SimulationLogger::SimulationLogger(const string filename) : m_filename(move(filename)) {

 	// open file
	m_out_file.open(m_filename);

	m_out_file <<"Time," <<"Entity,"<<"Event,"<<"Action," << "Rate," <<"HeartRate," << "BloodPressure,"<<"BloodVolume" << endl;

}



void SimulationLogger::LogData(double time, string entity, string event, string action, 				PhysiologyData& m_data) {

	m_out_file << time <<","<< entity <<","<< event <<","<< action <<","<<","<<
            m_data.heart_rate<<","<< m_data.pressure<<","<<m_data.blood_volume<<endl;



}



void SimulationLogger::LogRate(double time, string entity, string event, string action, double rate) {


	m_out_file << time <<","<< entity <<","<< event <<","<< action <<","<<
            rate<<endl;
}

//Destructor
SimulationLogger::~SimulationLogger() {

	m_out_file.close();

}
