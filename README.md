# CLAsim
The closed-loop assistant (CLA) simulation framework (CLAsim) enables simulation of the interaction between closed-loop medical systems consisting of patient monitors, closed-loop physiology management algorithms, and infusion pumps, and patient physiology. It is written in C++ and relies on the <a href="https://physiology.kitware.com/" target="_blank">Pulse Physiology Engine</a> for simulating patient physiology.

The current version is based on a demo example that we created for the <a href="https://gitlab.com/openconnectedmed/clasim/clasim_software_only/raw/master/Gessa-et-al-MCPS-2018-SIGBED-pub-version.pdf?inline=false" target="_blank">paper</a> we presented at the <a href="https://rtg.cis.upenn.edu/mcps-workshop-2018/#program" target="_blank">2018 Medical Cyber-Physical Systems Workshop</a>. We are working to make the framework easier to work with in terms of exploring other options beyond that demo scenario.

All files are licensed under the Apache 2.0 licence.

If you have any questions, email <a href="mailto:info@openconnectedmed.org" target="_blank">info@openconnectedmed.org</a>

## Getting Started

### Installing Pulse and CLAsim
CLAsim is has only been tested on Ubuntu 16.04. This is the recommended platform. We hope to support other platforms soon.

#### Pulse
To install, go to the <a href="https://gitlab.kitware.com/physiology/engine" target="_blank">Pulse repository</a> and follow the instructions there. Be sure to follow the instructions for Ubuntu.

If you need to install or update cmake, be sure to use the install from source option.

 *For Java, be sure to use the openjdk version.*

#### CLASim
1. Check out the CLAsim repository into a folder of your choice. We will refer to this folder below as `<PathToCLAsim>`
2. Install libconfig++  
   ~~~bash
   # update packages
   $ sudo apt-get update
   # install libconfig++
   $ sudo apt-get install libconfig++-dev
   ~~~
3. Find the paths for the libconfig.h++ and libconfig++.a
   1. find the path for libconfig.h++
      ~~~bash
      $ dpkg -L libconfig++-dev | grep libconfig.h++
      # output should be
      $ <somepath>/libconfig.h+
      # for example
      $ /usr/include/libconfig.h++
      ~~~
   2. find the path for libconfig++.a
      ~~~bash
      $ dpkg -L libconfig++-dev | grep libconfig++.a
      # output should be
      $ <somepath>/libconfig++.a
      # for example
      $ /usr/lib/libconfig++.a
      ~~~
4. Update the clasim.cmake file with libconfig++ information
   1. Go to `clasim.cmake` file which should be in `<PathToCLAsim>`
   2. Create copy of the file called `clasim.local.cmake`
   3. In `clasim.local.cmake`, follow steps 4 and 5.
   4. On line 6, which should looks like
      ~~~bash
      find_path(CONFIG++_INCLUDE_DIR libconfig.h++ <libconfig.h++ location>)
      ~~~
      replace `<libconfig.h++ location>` with the path (`<somepath>`) you found in step 3-1.
   5. On line 7, which should look like
      ~~~bash
      find_library(CONFIG++_LIBRARY NAMES libconfig++.a PATH <libconfig++.a location>)
      ~~~
      replace `<libconfig++.a location>` with the path (`<somepath>`) you found in step 3-2.
5. Install CLASim with CMake
   ~~~bash
   # make sure you are in the CLAsim folder
   $ cd <PathToCLAsim>
   # create a builds directory
   $ mkdir builds
   # enter builds folder
   $ cd builds
   # connect Pulse and CLASim for CMake Install
   # <PathToPulse> is the parent folder of the one you cloned Pulse into
   # that folder should contain the folders build and engine
   $ cmake .. -DCMAKE_PREFIX_PATH=<PathToPulse>/builds/install/
   # create CLASim add_executable
   $ make
   $ make Install
   ~~~
6. Test that the CLASim executable installed properly.
   ~~~bash
   # go to Pulse bin directory
   # (CLAsim executable installs there)
   $ cd <PathToPulse>/builds/install/bin
   # run CLAsim
   $ ./clasim
   ~~~

### Running Demo Examples
The repository comes with demo examples in `/simsrc/mcps2018`. The clasim.cmake file is already configured for `algo1` in this example. In the `resources` folder, you will find a `scenario.cfg` file. Here, you can adjust some of the simulation parameters.

Running step 6 above should run the simulation with the provided scenario file. The results will be generated in a `results` folder. The patient data will be in a `PulseSimEngine_<patient name>.txt` file.

### Plotting Results
In the `resources` folder, you will find a `plotresults.py` file. It uses many of the libraries found in the <a href="https://docs.anaconda.com/anaconda/install/linux/" target="_blank">Anaconda python distribution</a>. We recommend you use this distribution. Once you have this installed, you can test the script to make sure all the libraries are available, and then you can plot results as follows
~~~bash
# test the script
$ <PathToAnacondaPython> <PathToCLAsim>/resources/plotresults.py
# this should pring the following if everything loads properly
usage: script options patients
	options:
		-seperate	Plot all data
		-condense	Plot only patient data and pump action
# to plot make sure you are in the results folder of clasim
$ cd <PathToCLAsim>/results
# to plot the patient select a plot option one or more patients
# separate multiple patients by spaces
# possible patients are specified in the scenario.cfg file
# e.g. plotting separate with Hassan
$ <PathToAnacondaPython> <PathToCLAsim>/resources/plotresults.py -separate Hassan
~~~

## Coming Soon
We are working on the following:
- *Data visualization/interaction tools:* making the python scripts for plotting the data more flexible so others can use them to visualize/interact with data generated from the simulation.

- *Flexible output:* making it easier to specify where the results should go.

- *Flexible input:* making it easier to specify where the specific scenario file to use as well as making it easier to control more of the simulation from configuration files without the need to recompile.

- *Modulalar base code:* making the base code more modular so others can create and use their own models of devices and algorithms.
