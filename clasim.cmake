cmake_minimum_required(VERSION 3.9.4)

project(clasim)

# use dpkg -L libconfig++-dev to find the location of libconfig.h++ and libconfig++.a
find_path(CONFIG++_INCLUDE_DIR libconfig.h++ <libconfig.h++ location>)
find_library(CONFIG++_LIBRARY NAMES libconfig++.a PATH <libconfig++.a location>)

#clasim depends on pulse this will call will give you access to pulse directories
#needed by clasim
find_package(Pulse REQUIRED NO_MODULE)

# Variables to be used in the source files
set(CLASIM_CONFIG_DIR ${CMAKE_CURRENT_SOURCE_DIR}/resources/)
set(CLASIM_RESULTS_DIR ${CMAKE_CURRENT_SOURCE_DIR}/results/)
set(PULSE_STATE_DIR ${Pulse_INSTALL}/bin/states/)

# Create directory at build generation
file(MAKE_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/results)

# Generate header file to include variables
configure_file(
    ${CMAKE_CURRENT_SOURCE_DIR}/simsrc/mcps2018/include/configure_clasim_paths.h.in
    ${CMAKE_CURRENT_SOURCE_DIR}/simsrc/mcps2018/include/configure_clasim_paths.h
)

# *** set your simulation source directory
set(SIM_SRC_DIR simsrc/mcps2018/algo1)

# *** include your source files
# list each file individually
set(CLASIM_SOURCE_FILES

    ${SIM_SRC_DIR}/SimEngine.cpp
    ${SIM_SRC_DIR}/Monitor.cpp
    ${SIM_SRC_DIR}/Controller.cpp
    ${SIM_SRC_DIR}/Pump.cpp
    ${SIM_SRC_DIR}/SimulationLogger.cpp
    ${SIM_SRC_DIR}/MedicalDevice.cpp

)

add_executable(clasim ${CLASIM_SOURCE_FILES})

#include directories
target_include_directories(clasim PRIVATE ${Pulse_INCLUDE_DIRS})
target_include_directories(clasim PRIVATE ${CONFIG++_INCLUDE_DIR})

# *** include your simulation include directories here
target_include_directories(clasim PRIVATE simsrc/mcps2018/include/)

#libraries *** we use only the release libraries
target_link_libraries(clasim optimized "${Pulse_RELEASE_LIBS}")
target_link_libraries(clasim optimized "${CONFIG++_LIBRARY}")


install(TARGETS clasim RUNTIME DESTINATION ${Pulse_INSTALL}/bin)
